﻿This project isn't designed to run. I've just added some classes with some iOS methods that you might find useful at some stage.

The classes I've added are:

Camera.cs - some camera functions
ImageHandler.cs - processing images
Location.cs - handle location changes - calculate distances between points
PDF.cs - view a PDF
Reachability.cs (although this is Apple's) - connectivity check
UI.cs - handling screen sizes manually
Video.cs - play video
ZipFolder.cs - zip a folder