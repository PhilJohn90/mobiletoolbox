

using CoreGraphics;
using Foundation;
using UIKit;

namespace mobiletoolbox.iOS
{
    public class UI
    {
        public UI()
        {
        }

        //define a color
        public static UIColor myColor = UIColor.FromRGB(255, 255, 255);

        private void DoSomethingUniqueForDeviceSize()
        {
            CGRect frame = UIScreen.MainScreen.Bounds;

            if (frame.Width == 1366)
            {
                //12.9" ipad
            }
            else if (frame.Width == 1112)
            {
                //10.5" ipad
            }
            else if (frame.Width == 1194)
            {
                //11" ipad
            }
            else if (frame.Width == 1080)
            {
                //10.2" ipad
            }
            else if (frame.Width == 1024)
            {
                //9.7" ipad
            }
            else if (frame.Width == 896)
            {
                //iphone Xs Max, Xr
            }
            else if (frame.Width == 812)
            {
                //iphone Xs, X
            }
            else if (frame.Width == 736)
            {
                //iphone 7,8 Plus
            }
            else if (frame.Width == 667)
            {
                //iphone 6, 6s, 7, 8
            }
        }

        private void loadWebView(string path, string folder)
        {
            //load a webView
            webView.LoadFileUrl(new NSUrl("file://" + path), new NSUrl("file://" + folder));//folder is path - filename
            //webview is defined in a storyboard or nib

            //clearing a webView
            webView.EvaluateJavaScript("document.body.remove()", null);
        }

    }

}






