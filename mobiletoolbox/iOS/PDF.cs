
using System;
using System.IO;
using Foundation;

namespace mobiletoolbox.iOS

{

    public class PDF
    {
        public PDF()
        {
        }

        private async void ShowDocument(string path)
        {

            path = path.Replace(" ", "%20");
            string filename = System.IO.Path.GetFileName(path);
            string folder = path.Replace(filename, "");

            try
            {
                webView.LoadFileUrl(new NSUrl("file://" + path), new NSUrl("file://" + folder));
                //webview is defined in a storyboard or nib
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void SaveFile(byte[] fileBytes)//fileBytes is Base64
        {
            var docs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string path = docs + "MyFile.PDF";
            File.WriteAllBytes(path, fileBytes);

        }

    }

}


