
using System;
using AVFoundation;
using AVKit;
using CoreMedia;
using Foundation;
using UIKit;

namespace mobiletoolbox.iOS
{

    public class Video : UIViewController
    {
        public AVPlayer player;
        public AVPlayerViewController avpvc;
        private AVAsset asset;
        private AVPlayerItem playerItem;
        private NSTimer avTimer;

        private void PlayVideo(string path, int lastPlay)
        {
            CMTime startTime = CMTime.FromSeconds(lastPlay, 1);

            asset = AVAsset.FromUrl(NSUrl.FromFilename(path));
            playerItem = new AVPlayerItem(asset);
            playerItem.Seek(startTime);
            NSNotificationCenter.DefaultCenter.AddObserver(AVPlayerItem.DidPlayToEndTimeNotification, VideoDidFinishPlaying, playerItem);
            player = new AVPlayer(playerItem);

            avpvc = new AVPlayerViewController();
            avpvc.ShowsPlaybackControls = true;
            avpvc.Player = player;
            this.PresentViewController(avpvc, animated: true, completionHandler: null);
            player.Play();

            avTimer = NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(0.5), delegate {
                CheckRate();
            });

            avTimer.Fire();
        }

        private void VideoDidFinishPlaying(NSNotification notification)
        {
            avTimer.Invalidate();
            this.DismissModalViewController(true);
            int lastPlay = 0;
            //update a database with the last play
        }

        private void CheckRate()
        {
            CMTime currentTime = player.CurrentTime;
            Console.WriteLine(currentTime);

            if (player.Rate == 0)
            {
                //save the last currentTime
                double secs = currentTime.Seconds;
                int lastPlay = Convert.ToInt32(secs);
                //update a database with the last play
                avTimer.Invalidate();

            }
        }
    }
}



