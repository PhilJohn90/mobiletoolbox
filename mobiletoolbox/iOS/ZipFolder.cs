﻿using System;
using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;

namespace MobileToolbox.iOS
{
    public class ZipFolder
    {

        private string jsonStr;

        public async Task Zip(string lsUid, string folder)
        {
            

            var docs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string root = docs + "/" + folder;

            try
            {
                string zipFile = folder + ".zip";
                string personal = Environment.GetFolderPath(Environment.SpecialFolder.Personal); 
                string destinationPath = Path.Combine(personal, "../Library/"); // Library folder
                destinationPath = destinationPath + zipFile;

                if (File.Exists(destinationPath))
                {
                    File.Delete(destinationPath);
                }

                ZipFile.CreateFromDirectory(root, destinationPath);


                if (File.Exists(destinationPath))
                    Console.WriteLine("Done");
                else
                    Console.WriteLine("Failed");

            }
            catch (Exception ex)
            {

            }
            return;

        }
    }
}
