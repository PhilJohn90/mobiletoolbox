﻿using System;
using CoreLocation;
using Foundation;
using UIKit;

namespace mobiletoolbox.iOS
{
    public class Location
    {

		private CLLocationManager locationManager = null;
		private double userLatitude;
		private double userLongitude;

		public Location()
        {
        }

		private void StartLocationManager()
		{
			locationManager = new CLLocationManager();
			locationManager.AuthorizationChanged += (sender, args) =>
			{
				Console.WriteLine("Authorization changed to: {0}", args.Status);
			};
			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
				locationManager.RequestWhenInUseAuthorization();
			}

			locationManager.DesiredAccuracy = CLLocation.AccuracyBest;
			locationManager.StartUpdatingLocation();
			locationManager.LocationsUpdated += HandleLocationsUpdated;
		}

		public void HandleLocationsUpdated(object sender, CLLocationsUpdatedEventArgs e)
		{
			var location = e.Locations[e.Locations.Length - 1];
			userLatitude = location.Coordinate.Latitude;
			userLongitude = location.Coordinate.Longitude;
			NSUserDefaults.StandardUserDefaults.SetDouble(userLatitude, "UserLatitude");
			NSUserDefaults.StandardUserDefaults.SetDouble(userLongitude, "UserLongitude");
			NSUserDefaults.StandardUserDefaults.Synchronize();

		}

		public double GetSurfaceDistance(double lat1, double lon1, double lat2, double lon2)
		{//fromlat1,lon1 to lat2,lon2

			double a = (Math.Sin((lat1 - lat2) / 2));
			double b = Math.Cos(lat1);
			double c = Math.Cos(lat2);
			double d = Math.Sin((lon1 - lon2) / 2);

			double distance = 2 * Math.Asin(Math.Sqrt(Math.Pow(a, 2) + b * c * Math.Pow(d, 2)));

			distance = distance * 6366 * 0.621371192;

			//	Console.WriteLine("distance in miles " + distance);

			return distance;

		}

		public double RadiansFromDegrees(double num)
		{
			double inRads = num * Math.PI / 180;
			return inRads;
		}

		public double DegreesFromRadians(double num)
		{
			double inDegrees = num * 180 / Math.PI;
			return inDegrees;
		}



	}
}
