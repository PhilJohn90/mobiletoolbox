﻿using System;
using System.IO;
using System.Drawing;
using CoreGraphics;
using Foundation;
using UIKit;
using AVFoundation;


namespace MobileToolbox.iOS
{
    public class ImageHandler
    {

        public UIImage CropImage(UIImage sourceImage, int crop_x, int crop_y, int width, int height)
        {
            var imgSize = sourceImage.Size;
            UIGraphics.BeginImageContext(new SizeF(width, height));
            var context = UIGraphics.GetCurrentContext();
            var clippedRect = new RectangleF(0, 0, width, height);
            context.ClipToRect(clippedRect);
            var drawRect = new RectangleF(-crop_x, -crop_y, (float)imgSize.Width, (float)imgSize.Height);
            sourceImage.Draw(drawRect);
            var modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return modifiedImage;
        }

        public void ResizeAndSaveImage(string source, string destination)
        {
            UIImage sourceImage = new UIImage();
            sourceImage = UIImage.FromFile(source);
            var sourceSize = sourceImage.Size;
            float maxWidth = 140.0f;
            float maxHeight = 140.0f;
            var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
            var width = maxResizeFactor * sourceSize.Width;
            var height = maxResizeFactor * sourceSize.Height;
            UIGraphics.BeginImageContext(new CGSize(width, height));
            sourceImage.Draw(new CGRect(0, 0, width, height));
            var resultImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            // 0 is max compression (lowest quality), 1 is best quality
            using (NSData jpgImage = resultImage.AsJPEG(0.5f))
            {
                byte[] imageBytes = jpgImage.ToArray();
                File.WriteAllBytes(destination, imageBytes);
            }
        }

        public UIImage GenerateThumbnail(UIImage sourceImage)
        {
            var sourceSize = sourceImage.Size;
            float maxWidth = 140.0f;
            float maxHeight = 140.0f;
            var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
            var width = maxResizeFactor * sourceSize.Width;
            var height = maxResizeFactor * sourceSize.Height;
            UIGraphics.BeginImageContext(new CGSize(width, height));
            sourceImage.Draw(new CGRect(0, 0, width, height));
            var resultImage = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return resultImage;
        }

        public void GenerateAndSaveVideoThumbnail(string source, string destination)
        {
            var asset = AVAsset.FromUrl(NSUrl.FromFilename(source));
            var imageGenerator = AVAssetImageGenerator.FromAsset(asset);
            imageGenerator.AppliesPreferredTrackTransform = true;
            var actualTime = asset.Duration;
            CoreMedia.CMTime cmTime = new CoreMedia.CMTime(300, 60);

            NSError error;
            var imageRef = imageGenerator.CopyCGImageAtTime(cmTime, out actualTime, out error);

            UIImage sourceImage = new UIImage();
            if (imageRef != null)
            {
                UIImage playOverlay = new UIImage();
                playOverlay = UIImage.FromBundle("play_overlay");

                sourceImage = UIImage.FromImage(imageRef);
                var sourceSize = sourceImage.Size;
                float maxWidth = 140.0f;
                float maxHeight = 140.0f;
                var maxResizeFactor = Math.Max(maxWidth / sourceSize.Width, maxHeight / sourceSize.Height);
                var width = maxResizeFactor * sourceSize.Width;
                var height = maxResizeFactor * sourceSize.Height;
                UIGraphics.BeginImageContext(new CGSize(width, height));
                sourceImage.Draw(new CGRect(0, 0, width, height));
                playOverlay.Draw(new CGRect((width-144)/2, (height-144)/2, 144, 144));
                var resultImage = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();

                // 0 is max compression (lowest quality), 1 is best quality
                using (NSData jpgImage = resultImage.AsJPEG(0.5f))
                {
                    byte[] imageBytes = jpgImage.ToArray();
                    File.WriteAllBytes(destination, imageBytes);
                }
            }
        }

        public UIImage CaptureWebview(UIView viewToCapture)
        {
            UIGraphics.BeginImageContext(new SizeF(700f, 700f));
            viewToCapture.Layer.RenderInContext(UIGraphics.GetCurrentContext());
            UIImage image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();
            return image;
        }


    }
}
